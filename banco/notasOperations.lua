local M = {}
local lite = require('lsqlite3')
local db = lite.open('/home/regio/livro.sqlite')
require('minhaNota')

function M.notaPersistir(nota)
    local instrucao = db:prepare('INSERT INTO notas(idcapitulo, pagina, nota) VALUES (?,?,?);')
    instrucao:bind_values(nota.idCapitulo, nota.pagina, nota.nota)
    instrucao:step()
    instrucao:reset()
end

function M.buscaId(identificacao)
    local sel_stmt = db:prepare('SELECT * FROM notas WHERE id=?;')
    sel_stmt:bind_values(identificacao)
    sel_stmt:step()
    local result = sel_stmt:get_values()
    sel_stmt:finalize()
    local myNewNota = Notas:new()
    myNewNota.id = result[1]
    myNewNota.idCapitulo = result[2]
    myNewNota.pagina = result[3]
    myNewNota.nota = result[4]
    return myNewNota
end

function M.buscarTodos()
    local sel_stmt = 'SELECT * FROM notas;'
    local colecao = {}
    for linha in db:nrows(sel_stmt) do
        local item = Notas:new()
        item.id = linha.id
        item.idCapitulo = linha.idcapitulo
        item.pagina = linha.pagina
        item.nota = linha.nota
        table.insert(colecao, item)
    end
    return colecao
end

function M.buscarNotasDeCapitulo(idCapitulo)
    local sel_stmt = 'SELECT * FROM notas WHERE idcapitulo='..idCapitulo..';'
    local colecao = {}
    for linha in db:nrows(sel_stmt) do
        local item = Notas:new()
        item.id = linha.id
        item.idCapitulo = linha.idcapitulo
        item.pagina = linha.pagina
        item.nota = linha.nota
        table.insert(colecao, item)
    end
    return colecao
end

function M.buscarNotasComTexto(texto)
    local sel_stmt = 'SELECT * FROM notas WHERE nota LIKE "%'..texto..'%";'
    local colecao = {}
    for linha in db:nrows(sel_stmt) do
        local item = Notas:new()
        item.id = linha.id
        item.idCapitulo = linha.idcapitulo
        item.pagina = linha.pagina
        item.nota = linha.nota
        table.insert(colecao, item)
    end
    return colecao
end

return M