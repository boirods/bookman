local M = {}
local lite = require('lsqlite3')
local db = lite.open('/home/regio/livro.sqlite')
require('meuLivro')

function M.livroPersistir(livro)
    local instrucao = db:prepare('INSERT INTO livro(titulo, edicao, editora, autores) VALUES (?,?,?,?);')
    instrucao:bind_values(livro.titulo, livro.edicao, livro.editora, livro.autores)
    instrucao:step()
    instrucao:reset()
end

function M.buscaId(identificacao)
    local sel_stmt = db:prepare('SELECT * FROM livro WHERE id=?;')
    sel_stmt:bind_values(identificacao)
    sel_stmt:step()
    local result = sel_stmt:get_values()
    sel_stmt:finalize()
    local myNewLivro = Livro:new()
    myNewLivro.id = result[1]
    myNewLivro.titulo = result[2]
    myNewLivro.edicao = result[3]
    myNewLivro.editora = result[4]
    myNewLivro.autores = result[5]
    return myNewLivro
end

function M.buscarTodos()
    local sel_stmt = 'SELECT * FROM livro;'
    local colecao = {}
    for linha in db:nrows(sel_stmt) do
        local item = Livro:new()
        item.id = linha.id
        item.titulo = linha.titulo
        item.edicao = linha.edicao
        item.editora = linha.editora
        item.autores = linha.autores
        table.insert(colecao, item)
    end
    return colecao
end

function M.buscarTitulo(titulo)
    local sel_stmt = 'SELECT * FROM livro WHERE livro.titulo LIKE "%'.. titulo ..'%";'
    local colecao = {}
    for linha in db:nrows(sel_stmt) do
        local item = Livro:new()
        item.id = linha.id
        item.titulo = linha.titulo
        item.edicao = linha.edicao
        item.editora = linha.editora
        item.autores = linha.autores
        table.insert(colecao, item)
    end
    return colecao
end

function M.buscarEditora(editora)
    local sel_stmt = 'SELECT * FROM livro WHERE livro.editora LIKE "%'.. editora ..'%";'
    local colecao = {}
    for linha in db:nrows(sel_stmt) do
        local item = Livro:new()
        item.id = linha.id
        item.titulo = linha.titulo
        item.edicao = linha.edicao
        item.editora = linha.editora
        item.autores = linha.autores
        table.insert(colecao, item)
    end
    return colecao
end

function M.buscarEdicao(edicao)
    local sel_stmt = 'SELECT * FROM livro WHERE livro.edicao='.. edicao ..';'
    local colecao = {}
    for linha in db:nrows(sel_stmt) do
        local item = Livro:new()
        item.id = linha.id
        item.titulo = linha.titulo
        item.edicao = linha.edicao
        item.editora = linha.editora
        item.autores = linha.autores
        table.insert(colecao, item)
    end
    return colecao
end

function M.buscaAutores(autor)
    local sel_stmt = 'SELECT * FROM livro WHERE livro.autores LIKE "%'..autor..'%";'
    local colecao = {}
    for linha in db:nrows(sel_stmt) do
        local item = Livro:new()
        item.id = linha.id
        item.titulo = linha.titulo
        item.edicao = linha.edicao
        item.editora = linha.editora
        item.autores = linha.autores
        table.insert(colecao, item)
    end
    return colecao
end

function M.livroDeletar(identificacao)
    local instrucao = db:prepare('DELETE FROM livro WHERE livro.id=?;')
    instrucao:bind_values(identificacao)
    instrucao:step()
    instrucao:reset()
end

return M