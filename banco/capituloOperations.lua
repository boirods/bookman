local M = {}
local lite = require('lsqlite3')
local db = lite.open('/home/regio/livro.sqlite')

require 'meuCapitulo'

function M.capituloPersistir(capitulo)
    local instrucao = db:prepare('INSERT INTO capitulo(idLivro, pagina, titulo)VALUES (?,?,?);')
    instrucao:bind_values(capitulo.idLivro, capitulo.pagina, capitulo.titulo)
    instrucao:step()
    instrucao:reset()
end

function M.buscaCapId(identificacao)
    local sel_stmt = db:prepare('SELECT * FROM capitulo WHERE id=?;')
    sel_stmt:bind_values(identificacao)
    sel_stmt:step()
    local result = sel_stmt:get_values()
    sel_stmt:finalize()
    local myNewCapitulo = Capitulo:new()
    myNewCapitulo.id = result[1]
    myNewCapitulo.idLivro = result[2]
    myNewCapitulo.pagina = result[3]
    myNewCapitulo.titulo = result[4]
    return myNewCapitulo
end

function M.buscaTodos()
    local sel_stmt = 'SELECT * FROM capitulo;'
    local colecao = {}
    for linha in db:nrows(sel_stmt) do
        local item = Capitulo:new()
        item.id = linha.id
        item.titulo = linha.titulo
        item.idLivro = linha.idLivro
        item.pagina = linha.pagina
        table.insert(colecao, item)
    end
    return colecao
end

function M.buscaTodosCapLivroId(identificacao)
    local sel_stmt = 'SELECT * FROM capitulo WHERE capitulo.idLivro='.. identificacao ..';'
    local colecao = {}
    for linha in db:nrows(sel_stmt) do
        local item = Capitulo:new()
        item.id = linha.id
        item.titulo = linha.titulo
        item.idLivro = linha.idLivro
        item.pagina = linha.pagina
        table.insert(colecao, item)
    end
    return colecao
end

function M.buscaCapTitulo(titulo)
    local sel_stmt = 'SELECT * FROM capitulo WHERE capitulo.titulo LIKE "%'..titulo..'%";'
    local colecao = {}
    for linha in db:nrows(sel_stmt) do
        local item = Capitulo:new()
        item.id = linha.id
        item.titulo = linha.titulo
        item.idLivro = linha.idLivro
        item.pagina = linha.pagina
        table.insert(colecao, item)
    end
    return colecao
end

function M.removeCapId(identificacao)
    local instrucao = db:prepare('DELETE FROM capitulo WHERE capitulo.id=?;')
    instrucao:bind_values(identificacao)
    instrucao:step()
    instrucao:reset()
end

return M