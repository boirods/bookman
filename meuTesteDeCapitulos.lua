require 'meuCapitulo'
local modulo = require('banco.capituloOperations')

local meuCapitulo = Capitulo:new()
meuCapitulo.id      = 1
meuCapitulo.idLivro = 4
meuCapitulo.pagina  = 1990
meuCapitulo.titulo  = 'Pqp esqueci o titulo'

print(meuCapitulo:imprimir())
--modulo.capituloPersistir(meuCapitulo)

local capituloEspecifico = modulo.buscaCapId(1)
print('Capitulo com id = 1: ')
print(capituloEspecifico:imprimir())

local todosCap = modulo.buscaTodos()
print('Todos os capitulos de todos os livros')
for _,v in ipairs(todosCap) do v:imprimir() end
print()

local todosCapLua = modulo.buscaTodosCapLivroId(3)
print('Todos os capitulos do livro de Lua')
for _,v in ipairs(todosCapLua) do v:imprimir() end
print()

local todosCapComOs = modulo.buscaCapTitulo('os')
print('Todos os capitulos que tenham a string "os": ')
for _,v in ipairs(todosCapComOs) do v:imprimir() end

--local capADeletar = modulo.removeCapId(7)
