local M = {}

function M.separaAutores(autores)
		local auts = mysplit(autores, ';')
		return auts
end

function mysplit (inputstr, sep)
  	if sep == nil then
      	sep = "%s"
    end
    local t={}
    for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
        table.insert(t, str)
    end
    return t
end

return M