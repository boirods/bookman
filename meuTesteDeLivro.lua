require('meuLivro')
require('meuCapitulo')
require('minhaNota')
local module = require('banco.livroOperations')

local meuLivroLua = Livro:new()
meuLivroLua.titulo  = 'Meu livro imaginario'
meuLivroLua.edicao  = 199
meuLivroLua.editora = 'Imagine books'
meuLivroLua.autores = 'Rodrigo Régio; Renato Régio;Vanda Maria Régio;Francisco Régio Filho'

print(meuLivroLua:imprimir())
--module.livroPersistir(meuLivroLua)

print('Buscando o livro de id = 1')
local meuLivroEspecifico = module.buscaId(1)--dado vindo do banco
print(meuLivroEspecifico:imprimir())

print('Todos livros no banco: ')
local livros = module.buscarTodos()
for _,v in ipairs(livros) do v:imprimir() end
print()

print('Todos livros no banco que tenham a letra "a" no titulo: ')
local livrosComA = module.buscarTitulo('a')
for _,v in ipairs(livrosComA) do v:imprimir() end
print()

print('Todos os livros no banco que tenham a editora com a letra "l"')
local editorasComL = module.buscarEditora('l')
for _,v in ipairs(editorasComL) do v:imprimir() end
print()

print('Todos os livros no banco que tenham a primeira edição')
local livrosNaPrimeiraEdicao = module.buscarEdicao(1)
for _,v in ipairs(livrosNaPrimeiraEdicao) do v:imprimir() end
print()

print('Todos os livros no banco que tenham como autor algo como "Ieru":')
local livrosComAutor = module.buscaAutores('ieru')
for _,v in ipairs(livrosComAutor) do v:imprimir() end
print()

--local teste = module.livroDeletar(5)