require 'minhaNota'

local modulo = require 'banco.notasOperations'
local minhaNota = Notas:new()
minhaNota.idCapitulo = 4
minhaNota.pagina = 15
minhaNota.nota = 'O autor dá alguns conceitos sobre os mecanismos de segurança como honeypot que engana o invasor fazendo-o pensar que está invadindo o sistema.'

print(minhaNota:imprimir())
--modulo.notaPersistir(minhaNota)

local minhaNotaEspecifica = modulo.buscaId(1)
print(minhaNotaEspecifica:imprimir())

local todasMinhasNotas = modulo.buscarTodos()
print('Todas as minhas notas!')
for _, value in ipairs(todasMinhasNotas) do value:imprimir() end

local todasNotasDoCapituloDeLua = modulo.buscarNotasDeCapitulo(2)
print('\nTodas as notas do capitulo 1 do livro de Lua!')
for _,value in ipairs(todasNotasDoCapituloDeLua) do value:imprimir() end

local todasNotasComTextoEngana = modulo.buscarNotasComTexto('engana')
print('\nBuscando todas notas que tenham o texto "engana"!')
for _,value in ipairs(todasNotasComTextoEngana) do value:imprimir() end