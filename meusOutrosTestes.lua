local modulo = require('helpers.separaAutores')

local autoresTeste = 'Rodrigo Régio;Eleno Nogueira;Rosalice Senna'
local autoresTeste2 = 'Renato Régio de Araújo'

print('Valores originais:\n'..autoresTeste..'\n'..autoresTeste2..'\n')

for _,v in ipairs(modulo.separaAutores(autoresTeste)) do print(v) end
print('\nOutro vetor')
for _,v in ipairs(modulo.separaAutores(autoresTeste2)) do print(v) end
